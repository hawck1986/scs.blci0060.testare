package BibliotecaApp.model;

import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {

    Carte c;
    Carte c1;

    @BeforeClass
    public static void setup() throws Exception {

        System.out.println("Before any test ====@BeforeClass======");

    }

    @AfterClass
    public static void teardown() throws Exception {

        System.out.println("After all tests ========@AfterClass========");
    }


    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("Valea frumoasei!!!!");
        System.out.println("before test ---@Before----");
        c.setEditura("Humanitas");
        c.setAnAparitie("2000");

    }

    @After
    public void tearDown() throws Exception {
        c =null;
        System.out.println("after test ----@After-----");
    }

    @Test
    public void getTitlu() {

        assertEquals("Valea frumoasei!!!!", c.getTitlu());

    }

    @Test
    public void setTitlu() {
        c.setTitlu("Povestiri nemuritoare");
        assertEquals("Povestiri nemuritoare", c.getTitlu());

    }


    @Test(timeout = 10)
    public void getCarti() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(c.getEditura(),"Humanitas");
    }


    @Test(expected = Exception.class)
    public void setAnAparitie() throws Exception{
        c.setAnAparitie("1899");
    }


    //tema

    @Test(timeout = 15)
    public  void getCarte(){
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(c.getTitlu(),"Valea frumoasei!!!!");
    }


    @Test
    public void getEditura(){
        assertEquals("Humanitas", c.getEditura());
    }


    @Test
    public void getAnAparitie(){
        assertEquals("2000",c.getAnAparitie());
    }

    @Test
    public void setEditura(){
        List<String> autori= new ArrayList<>();
        autori.add("Eminescu");
        c.setAutori(autori);

        assertEquals("[Eminescu]", c.getAutori().toString());
    }




}